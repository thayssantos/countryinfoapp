package com.awesomeapps.countryinfoapp.util.glide;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.widget.ImageView;

import com.awesomeapps.countryinfoapp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;

/**
 * Created by DELL on 07/11/2017.
 */

public class SvgLoader {

    private static RequestBuilder<PictureDrawable> requestBuilder;

    public static void loadImage(String flag, ImageView ivFlag, Context context) {

        requestBuilder = Glide.with(context)
                .as(PictureDrawable.class)
                .transition(withCrossFade())
                .load(R.mipmap.ic_placeholder)
                .listener(new SvgSoftwareLayerSetter());

        Uri uri = Uri.parse(flag);
        requestBuilder.load(uri).into(ivFlag);

    }
}
