package com.awesomeapps.countryinfoapp.util.mvp;

/**
 * Created by Thays Santos Duarte on 02/11/2017.
 */

public class BasePresenter<ViewT> implements IBasePresenter<ViewT> {

    protected ViewT view;

    @Override
    public void onViewActive(ViewT view) {
        this.view = view;
    }

    @Override
    public void onViewInactive() {
        view = null;
    }
}
