package com.awesomeapps.countryinfoapp.util.common;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public final class Constants {
    public static final String APPLICATION_FOLDER = "/countryinfoapp";
    public static final String FLAG_NAME = "flag";

    public static final String GET_COUNTRIES_ENDPOINT = "rest/v2/name/";

    public static final String BUNDLE_KEY_COUNTRY_RESULT = "bundle_key_country_result";
}
