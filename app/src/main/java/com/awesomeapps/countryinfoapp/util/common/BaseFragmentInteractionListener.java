package com.awesomeapps.countryinfoapp.util.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by Thays Santos Duarte on 02/11/2017.
 */

public interface BaseFragmentInteractionListener {
    <T extends Fragment> void showFragment(Class<T> fragmentClass, Bundle bundle,
            boolean addToBackStack);
}
