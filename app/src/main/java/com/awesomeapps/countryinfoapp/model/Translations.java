package com.awesomeapps.countryinfoapp.model;

import java.io.Serializable;

/**
 * Created by Thays Santos Duarte on 07/11/2017.
 */

public class Translations implements Serializable {
    private static final long serialVersionUID = 1L;
    private String de;

    public String getDe() {
        return de;
    }

    public void setDe(String de) {
        this.de = de;
    }
}
