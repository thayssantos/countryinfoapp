package com.awesomeapps.countryinfoapp.model;

import java.io.Serializable;

/**
 * Created by Thays Santos Duarte on 07/11/2017.
 */

public class Language implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String nativeName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }
}
