package com.awesomeapps.countryinfoapp.ui.countrysearch;

import android.util.Log;

import com.awesomeapps.countryinfoapp.R;
import com.awesomeapps.countryinfoapp.data.DataManager;
import com.awesomeapps.countryinfoapp.data.GetCountriesCallback;
import com.awesomeapps.countryinfoapp.model.Country;
import com.awesomeapps.countryinfoapp.util.mvp.BasePresenter;

import java.util.List;

/**
 * Created by Thays Santos Duarte on 07/11/2017.
 */

class CountrySearchPresenter extends BasePresenter<CountrySearchContract.View> implements
        CountrySearchContract.Presenter {
    private final String TAG = getClass().getSimpleName();
    private DataManager dataManager;

    public CountrySearchPresenter(CountrySearchContract.View view, DataManager dataManager) {
        this.view = view;
        this.dataManager = dataManager;
    }

    @Override
    public void getCountries(String query) {
        if (view != null) {
            dataManager.getCountries(view.getContext(), query, new GetCountriesCallback() {
                @Override
                public void onSuccess(List<Country> countries) {
                    view.showCountries(countries);
                }

                @Override
                public void onFailure(Throwable throwable) {
                    Log.e(TAG, throwable.getMessage());
                    view.showToastMessage(view.getContext().getResources().getString(R.string.not_found));
                }

                @Override
                public void onNetworkFailure() {

                }
            });
        }
    }
}
