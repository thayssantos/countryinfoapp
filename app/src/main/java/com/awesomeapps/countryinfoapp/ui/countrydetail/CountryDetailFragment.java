package com.awesomeapps.countryinfoapp.ui.countrydetail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.awesomeapps.countryinfoapp.R;
import com.awesomeapps.countryinfoapp.model.Country;
import com.awesomeapps.countryinfoapp.util.common.BaseFragmentInteractionListener;
import com.awesomeapps.countryinfoapp.util.common.Constants;
import com.awesomeapps.countryinfoapp.util.glide.SvgLoader;
import com.awesomeapps.countryinfoapp.util.mvp.BaseView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Thays Santos Duarte on 07/11/2017.
 */

public class CountryDetailFragment extends BaseView implements OnMapReadyCallback {
    private BaseFragmentInteractionListener fragmentInteractionListener;
    private Country countriesResult;
    private ImageView flag;
    private TextView name;
    private TextView nativeName;
    private TextView capital;
    private TextView area;
    private TextView translationGerman;
    private GoogleMap map;
    private MapFragment mapFragment;
    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            countriesResult =
                    (Country) getArguments().getSerializable(Constants.BUNDLE_KEY_COUNTRY_RESULT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_country_detail, container, false);

            flag = (ImageView) view.findViewById(R.id.iv_flag_country);
            name = (TextView) view.findViewById(R.id.tv_country_name);
            nativeName = (TextView) view.findViewById(R.id.tv_country_native_name);
            capital = (TextView) view.findViewById(R.id.tv_capital);
            area = (TextView) view.findViewById(R.id.tv_area);
            translationGerman = (TextView) view.findViewById(R.id.tv_translations);

            mapFragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(
                    R.id.map);
            mapFragment.getMapAsync(this);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SvgLoader.loadImage(countriesResult.getFlag(), flag, getActivity());

        name.setText(getResources().getString(R.string.name) + ": " + countriesResult.getName());
        nativeName.setText(getResources().getString(R.string.native_name) + ": "
                + countriesResult.getNativeName());
        capital.setText(
                getResources().getString(R.string.capital) + ": " + countriesResult.getCapital());
        area.setText(getResources().getString(R.string.area) + ": " + countriesResult.getArea());
        translationGerman.setText(
                getResources().getString(R.string.translation) + ": "
                        + countriesResult.getTranslations().getDe() + " "
                        + getResources().getString(R.string.translation_german));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentInteractionListener = (BaseFragmentInteractionListener) getActivity();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        LatLng place = new LatLng(countriesResult.getLatlng().get(0),
                countriesResult.getLatlng().get(1));
        map.addMarker(new MarkerOptions().position(place));
        map.moveCamera(CameraUpdateFactory.zoomTo(4.0f));
        map.moveCamera(CameraUpdateFactory.newLatLng(place));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment mapFragment = (MapFragment) getActivity()
                .getFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            getActivity().getFragmentManager().beginTransaction()
                    .remove(mapFragment).commit();
        }
    }
}
