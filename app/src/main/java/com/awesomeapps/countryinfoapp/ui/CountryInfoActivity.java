package com.awesomeapps.countryinfoapp.ui;

import android.os.Bundle;

import com.awesomeapps.countryinfoapp.R;
import com.awesomeapps.countryinfoapp.ui.countrysearch.CountrySearchFragment;
import com.awesomeapps.countryinfoapp.util.BaseActivity;
import com.awesomeapps.countryinfoapp.util.common.BaseFragmentInteractionListener;

/**
 * Created by Thays Santos Duarte on 07/11/2017.
 */

public class CountryInfoActivity extends BaseActivity implements BaseFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_info);
        showFragment(CountrySearchFragment.class);
    }
}
