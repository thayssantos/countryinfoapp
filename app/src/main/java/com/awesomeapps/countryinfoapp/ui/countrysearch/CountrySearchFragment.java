package com.awesomeapps.countryinfoapp.ui.countrysearch;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.awesomeapps.countryinfoapp.R;
import com.awesomeapps.countryinfoapp.data.DataManager;
import com.awesomeapps.countryinfoapp.model.Country;
import com.awesomeapps.countryinfoapp.ui.countrydetail.CountryDetailFragment;
import com.awesomeapps.countryinfoapp.util.common.BaseFragmentInteractionListener;
import com.awesomeapps.countryinfoapp.util.common.Constants;
import com.awesomeapps.countryinfoapp.util.mvp.BaseView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Thays Santos Duarte on 07/11/2017.
 */

public class CountrySearchFragment extends BaseView implements CountrySearchContract.View {
    private List<Country> countries;
    private CountrySearchContract.Presenter presenter;
    private View view;
    private RecyclerView countriesList;
    private EditText searchField;
    private CountriesResultRecyclerAdapter countriesResultRecyclerAdapter;
    private BaseFragmentInteractionListener fragmentInteractionListener;
    private String query = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        countries = new ArrayList<>();
        presenter = new CountrySearchPresenter(this, DataManager.getInstance());
        DataManager.getInstance().init(getActivity().getCacheDir());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_country_search, container, false);

        searchField = (EditText) view.findViewById(R.id.et_search_field);
        countriesList = (RecyclerView) view.findViewById(R.id.rv_countries_list);

        return view;
    }

    private void clearInfo() {
        countriesResultRecyclerAdapter.clearList();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        countriesList.setLayoutManager(linearLayoutManager);
        countriesResultRecyclerAdapter = new CountriesResultRecyclerAdapter(this, countries);
        countriesList.setAdapter(countriesResultRecyclerAdapter);

        createDebounceEditText(searchField);
    }

    @Override
    public void showCountries(List<Country> countries) {
        countriesResultRecyclerAdapter.addCountries(countries);
        hideKeyboard();
    }

    @Override
    public void showToastMessage(String message) {
        super.showToastMessage(message);
    }

    private void hideKeyboard() {
        if (getContext() != null && getContext().INPUT_METHOD_SERVICE != null) {
            InputMethodManager imm = (InputMethodManager) getActivity()
                    .getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showDetailFragment(int countryPosition) {
        Country country = countries.get(countryPosition);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_KEY_COUNTRY_RESULT, country);
        fragmentInteractionListener.showFragment(CountryDetailFragment.class, bundle, true);
    }

    private void createDebounceEditText(EditText searchField) {
        fromView(searchField)
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(String text) throws Exception {
                        if (text.isEmpty()) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                })
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String result) throws Exception {
                        if (!result.equals(query)) {
                            query = result;
                        }
                        clearInfo();
                        presenter.getCountries(result);
                    }
                });
    }

    public Observable<String> fromView(EditText editText) {

        final PublishSubject<String> subject = PublishSubject.create();
        subject.onNext(editText.getText().toString());

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subject.onNext(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        return subject;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        fragmentInteractionListener = (BaseFragmentInteractionListener) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
