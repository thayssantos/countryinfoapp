package com.awesomeapps.countryinfoapp.ui.countrysearch;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.awesomeapps.countryinfoapp.R;
import com.awesomeapps.countryinfoapp.model.Country;
import com.awesomeapps.countryinfoapp.util.glide.SvgLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thays Santos Duarte on 07/11/2017.
 */

class CountriesResultRecyclerAdapter extends
        RecyclerView.Adapter<CountriesResultRecyclerAdapter.ViewHolder> {
    private CountrySearchFragment fragment;
    private List<Country> countriesResult;

    public CountriesResultRecyclerAdapter(CountrySearchFragment fragment,
            List<Country> countriesResult) {
        this.countriesResult = new ArrayList<>();
        this.fragment = fragment;
        this.countriesResult = countriesResult;
    }

    @Override
    public CountriesResultRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
            int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_country_result, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CountriesResultRecyclerAdapter.ViewHolder viewHolder,
            final int position) {
        Country country = countriesResult.get(position);

        viewHolder.tvCountry.setText(country.getName());

        SvgLoader.loadImage(country.getFlag(), viewHolder.ivFlag, fragment.getContext());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.showDetailFragment(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return countriesResult.size();
    }

    public void addCountries(List<Country> countries) {
        countriesResult.addAll(countries);
        notifyDataSetChanged();
    }

    public void clearList() {
        countriesResult.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View itemView;
        private TextView tvCountry;
        private ImageView ivFlag;

        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            tvCountry = (TextView) itemView.findViewById(R.id.tv_country);
            ivFlag = (ImageView) itemView.findViewById(R.id.iv_flag);
        }
    }
}
