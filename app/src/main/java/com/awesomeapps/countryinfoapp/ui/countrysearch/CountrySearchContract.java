package com.awesomeapps.countryinfoapp.ui.countrysearch;

import com.awesomeapps.countryinfoapp.model.Country;
import com.awesomeapps.countryinfoapp.util.mvp.IBasePresenter;
import com.awesomeapps.countryinfoapp.util.mvp.IBaseView;

import java.util.List;

/**
 * Created by Thays Santos Duarte on 07/11/2017.
 */

interface CountrySearchContract {

    interface View extends IBaseView {
        void showCountries(List<Country> countries);
    }

    interface Presenter extends IBasePresenter<View> {
        void getCountries(String query);
    }
}
