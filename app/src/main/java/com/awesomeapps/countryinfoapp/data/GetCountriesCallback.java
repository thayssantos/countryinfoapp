package com.awesomeapps.countryinfoapp.data;

import com.awesomeapps.countryinfoapp.model.Country;

import java.util.List;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public interface GetCountriesCallback {

    void onSuccess(List<Country> countries);

    void onFailure(Throwable throwable);

    void onNetworkFailure();
}
