package com.awesomeapps.countryinfoapp.data;

import com.awesomeapps.countryinfoapp.model.Country;
import com.awesomeapps.countryinfoapp.util.common.Constants;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public interface CountriesApiService {

    @GET(Constants.GET_COUNTRIES_ENDPOINT + "{query}")
    Observable<List<Country>> getCountriesSearch(
            @Path("query") String query);
}
