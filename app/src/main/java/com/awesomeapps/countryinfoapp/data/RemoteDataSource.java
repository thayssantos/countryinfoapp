package com.awesomeapps.countryinfoapp.data;

import android.util.Log;

import com.awesomeapps.countryinfoapp.BuildConfig;
import com.awesomeapps.countryinfoapp.model.Country;

import java.io.File;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public class RemoteDataSource {

    private final String TAG = getClass().getSimpleName();
    private Retrofit retrofitClient;
    private CountriesApiService countriesApiService;
    private CompositeDisposable compositeDisposable;

    public RemoteDataSource() {

    }

    public void init(File file) {
        retrofitClient = createRetrofit(file);
        countriesApiService = retrofitClient.create(CountriesApiService.class);
        compositeDisposable = new CompositeDisposable();
    }

    public void getCountries(String query, GetCountriesCallback getCountriesCallback) {
        compositeDisposable.add(createObservable(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(createObserver(getCountriesCallback)));
    }

    private Observable<List<Country>> createObservable(String query) {
        return countriesApiService.getCountriesSearch(query);
    }

    private DisposableObserver<List<Country>> createObserver(
            final GetCountriesCallback getCountriesCallback) {
        return new DisposableObserver<List<Country>>() {
            @Override
            public void onNext(List<Country> countries) {
                getCountriesCallback.onSuccess(countries);
            }

            @Override
            public void onError(Throwable e) {
                getCountriesCallback.onFailure(e);
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "onComplete");
            }
        };
    }

    private Retrofit createRetrofit(File file) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.REST_COUNTRIES_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(createOkHttpClient(file))
                .build();
    }

    private OkHttpClient createOkHttpClient(File file) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        int cacheSize = 10 * 1024 * 1024; // 10 MB
        Cache cache = new Cache(file, cacheSize);

        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder().cache(cache);
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }
}
