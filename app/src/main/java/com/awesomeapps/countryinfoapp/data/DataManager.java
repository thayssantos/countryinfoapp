package com.awesomeapps.countryinfoapp.data;

import android.content.Context;

import java.io.File;

/**
 * Created by Thays Santos Duarte on 03/11/2017.
 */

public class DataManager implements IDataManager {
    private RemoteDataSource remoteDataSource;
    private static DataManager instance = new DataManager();

    public static DataManager getInstance() {
        return instance;
    }

    private DataManager() {
        remoteDataSource = new RemoteDataSource();
    }

    public void init(File file) {
        remoteDataSource.init(file);
    }

    @Override
    public void getCountries(Context context, String query,
            GetCountriesCallback getCountriesCallback) {
        remoteDataSource.getCountries(query, getCountriesCallback);
    }

    public void destroyInstance() {
        remoteDataSource = null;
    }
}
