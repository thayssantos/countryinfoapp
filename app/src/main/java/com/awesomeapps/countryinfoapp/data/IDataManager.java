package com.awesomeapps.countryinfoapp.data;

import android.content.Context;

/**
 * Created by Thays Santos Duarte on 04/11/2017.
 */

public interface IDataManager {

    void getCountries(Context context, String query, GetCountriesCallback getCountriesCallback);
}
