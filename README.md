# README #

App to search countries and informations about them.

### Android Studio Version ###

* 3.0

### External Dependencies ###

* Rxjava2
* Retrofit2
* Glide
* Android SVG

### Cache ###

* A bonus of the project was include cache of requests
* The external frameworks Glide e Retrofit provide an option to create a cache of requests
